declare namespace reign {
    interface HeaderProps {
        title: string;
        subtitle: string;
    }

    interface Article {
        _id: number;
        author?: string;
        comment_text?: string;
        created_at: string;
        deleted: boolean;
        num_comments?: number;
        objectID: string;
        parent_id: number;
        points?: number;
        story_id?: number;
        story_text?: string;
        story_title?: string;
        story_url?: string;
        title?: string;
        url?: string;
    }

    interface ArticleProps {
        article: IArticle;
        onDelete: Function;
    }

    interface Response {
        statusCode: number
        message: string
    }
}

declare module 'reign-challenge' {
    export = reign
}