import React from 'react';
import Articles from './views/Articles';

function App() {
  return (
    <>
      <Articles/>
    </>
  );
}

export default App;
