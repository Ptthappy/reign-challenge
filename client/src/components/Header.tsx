import React from 'react';
import { HeaderProps } from 'reign-challenge';
import './Header.css'

const Header: React.FC<HeaderProps> = ({ title, subtitle }) => {
  return (
    <div className="header">
      <span className="title">{title}</span>
      <span className="subtitle">{subtitle}</span>
    </div>
  );
}

export default Header;
