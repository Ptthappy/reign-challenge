import React from 'react';
import './Article.css'
import { ArticleProps } from 'reign-challenge';
import Trashcan from '../assets/icons/trash.svg';
import { DateTime } from 'luxon';

const Article: React.FC<ArticleProps> = ({ article, onDelete}) => {
    const handleRedirect = async (e: React.MouseEvent<HTMLElement>) => {
        if(article.story_url || article.url) {
            window.open(article.story_url || article.url, '_blank')
            return;
        }
        // handle when both story_url and url are null
    }

    const handleDelete = async (e: React.MouseEvent<HTMLElement>) => {
        e.stopPropagation();
        onDelete(article._id);
    }
    
    const getDate = () => {
        return DateTime.now().minus({ days: 1 }) < DateTime.fromISO(article.created_at) ?
            DateTime.fromISO(article.created_at).toLocaleString(DateTime.TIME_SIMPLE)
            : DateTime.fromISO(article.created_at).toRelativeCalendar()
    }

    return (
        <div className="article" onClick={handleRedirect}>
            <div className="inner-article">
                <div className="content-row flex-1">
                    <span className="title-span">{article.story_title || article.title}</span>
                    <span className="author-span">-</span>
                    <span className="author-span">{article.author}</span>
                    <span className="author-span">-</span>
                </div>
                <div className="content-row">
                    <span className="title-span mr-5vw capitalize">{getDate()}</span>
                    <div className="icon-container" onClick={handleDelete}>
                        <img src={Trashcan} className="icon trashcan-icon" />
                    </div>
                </div>
            </div>
        </div>
        )
    }
    
    export default Article;