import React, { useEffect } from 'react';
import './Articles.css'
import Header from '../components/Header';
import Article from '../components/Article';
import { Article as IArticle } from 'reign-challenge';
import { deleteArticle, getArticles } from '../services/articles';
import { useState } from 'react';


const Articles: React.FC = () => {
  const [data, setData] = useState<IArticle[]>([]);
  const [loading, setLoading] = useState<boolean>(false); //to handle UI when a request is pending

  useEffect(() => {
    setLoading(true);
    fetchArticles();
  }, []);

  const fetchArticles = async () => {
    try {
      const data = await getArticles();
      setData(data.articles);
    } catch(e) {
      console.log(e);
      // display error
    } finally {
      setLoading(false);
    }
  }

  const removeArticle = async (id: number) => {
    setLoading(true);
    try {
      await deleteArticle(id);
    } catch(e) {
      console.log(e);
      // display error
    } finally {
      fetchArticles();
    }
  }

  return (
    <div className="view-container">
      {/* {loading && <div></div>} */} {/* actual loading action */}
      <Header title='Hacker News Feed' subtitle='We <3 hacker news!' />
      {data.map((d: IArticle, i: number) => {
        if(!d.story_title && !d.title) return null;
        return (
          <Article key={i} article={d} onDelete={removeArticle} />
        )
       }
      )}
    </div>
  );
}

export default Articles;