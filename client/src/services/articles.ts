import axios from "axios";
import { Article } from "reign-challenge";
import { Response } from "reign-challenge";

export const getArticles = async (): Promise<Response & { articles: Article[] }> => {
    return (await axios.get<Response & { articles: Article[] }>('/articles')).data;
}

export const deleteArticle = async (id: number): Promise<any> => {
    return (await axios.delete(`/articles/${id}`)).data;
}