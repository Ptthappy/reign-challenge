import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression, Interval, Timeout } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { Article, ArticleDocument } from './articles.schema';

@Injectable()
export class ArticlesService {
  constructor(
    private httpService: HttpService,
    @InjectModel(Article.name) private model: Model<ArticleDocument>,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    await this.fetchArticles();
  }

  @Cron(CronExpression.EVERY_HOUR)
  async handleRefresh(): Promise<void> {
    await this.fetchArticles();
    console.log('Articles refreshed');
  }

  async fetchArticles(): Promise<void> {
    await this.httpService
      .get<{ hits: Article[] }>(
        'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      )
      .subscribe((res) => {
        res.data.hits.forEach(async (article, i) => {
          article._id = parseInt(article.objectID);
          await this.model.findOneAndUpdate({ _id: article._id }, article, {
            upsert: true,
            setDefaultsOnInsert: true,
          });
        });
      });
  }

  async findAll(): Promise<Article[]> {
    return await this.model
      .find({ deleted: false })
      .sort({ created_at: -1 })
      .exec();
  }

  async remove(id: number): Promise<boolean> {
    const res = await this.model
      .updateOne({ _id: id }, { deleted: true })
      .exec();
    return res.nModified === 1;
  }
}
