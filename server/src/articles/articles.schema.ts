import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop() _id: number;

  @Prop() created_at: string;

  @Prop() title: string;

  @Prop() url: string;

  @Prop() author: string;

  @Prop() comment_text: string;

  @Prop() story_id: number;

  @Prop() story_title: string;

  @Prop() story_url: string;

  @Prop() num_comments: number;

  @Prop() points: number;

  @Prop() parent_id: number;

  @Prop() objectID: string;

  @Prop({ default: false }) deleted: boolean;

  @Prop() story_text: string;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
