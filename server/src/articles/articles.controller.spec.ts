import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';

describe('ArticlesModule', () => {
  let controller: ArticlesController;
  const service = {
    findAll: () => ['test'],
    remove: (id: number) => id === 1, // Supposing an item with id 1 exists and everything else doesn't
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticlesController],
      providers: [
        {
          provide: ArticlesService,
          useValue: service,
        },
      ],
    }).compile();

    controller = module.get<ArticlesController>(ArticlesController);
  });

  it('should return an array of articles', async () => {
    const res = await controller.findAll();
    expect(res.statusCode).toBe(200);
    expect(res.articles).toStrictEqual(service.findAll());
  });

  it('should delete an article', async () => {
    const res = await controller.remove(1);
    expect(res.statusCode).toBe(200);
    expect(res.message).toBe('Article deleted successfully');
  });

  it('should return with already deleted message', async () => {
    const res = await controller.remove(10);
    expect(res.statusCode).toBe(200);
    expect(res.message).toBe('Article has been already deleted');
  });
});
