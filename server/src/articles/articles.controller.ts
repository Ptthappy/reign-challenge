import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { Article } from './articles.schema';
import { ArticlesService } from './articles.service';
import { Response } from 'reign-challenge';

@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @Get()
  async findAll(): Promise<Response & { articles?: Article[] }> {
    try {
      const articles = await this.articlesService.findAll();
      return {
        message: 'Articles retrieved successfully',
        statusCode: 200,
        articles,
      };
    } catch (e) {
      return {
        message: 'An error ocurred while retrieving articles',
        statusCode: 500,
      };
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string | number): Promise<Response> {
    try {
      return (await this.articlesService.remove(+id))
        ? {
            message: 'Article deleted successfully',
            statusCode: 200,
          }
        : {
            message: 'Article has been already deleted',
            statusCode: 200,
          };
    } catch (e) {
      return {
        message: 'An error ocurred while deleting the specified article',
        statusCode: 500,
      };
    }
  }
}
