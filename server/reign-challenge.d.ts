declare namespace reign {
    interface Response {
        statusCode: number
        message: string
    }
}

declare module 'reign-challenge' {
    export = reign;
}