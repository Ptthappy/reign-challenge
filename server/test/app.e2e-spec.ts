import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { ArticlesService } from '../src/articles/articles.service';
import { ArticlesController } from '../src/articles/articles.controller';

describe('ArticlesController (e2e)', () => {
  let app: INestApplication;
  const service = {
    findAll: () => ['test'],
    remove: (id: number) => id === 1, // Supposing an item with id 1 exists and everything else doesn't
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [ArticlesController],
      providers: [
        {
          provide: ArticlesService,
          useValue: service,
        },
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/articles (GET)', async () => {
    const response = await request(app.getHttpServer()).get('/articles');
    expect(response.status).toBe(200);
    expect(response.body.statusCode).toBe(200);
    expect(response.body.articles).toStrictEqual(service.findAll());
    return;
  });

  it('/articles (DELETE) (Existent Item)', async () => {
    const response = await request(app.getHttpServer()).delete('/articles/1');
    expect(response.status).toBe(200);
    expect(response.body.statusCode).toBe(200);
    expect(response.body.message).toBe('Article deleted successfully');
  });

  it('/articles (DELETE) (Non-existent Item', async () => {
    const response = await request(app.getHttpServer()).delete('/articles/89');
    expect(response.status).toBe(200);
    expect(response.body.statusCode).toBe(200);
    expect(response.body.message).toBe('Article has been already deleted');
  });
});
