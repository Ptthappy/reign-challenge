# Running the proyect

In the proyect directory, run:

### `docker-compose up`

It should initializate all the containers running the client, server and mongoDB.

By default the client runs on the port 4200, server on 3000 and mongoDB on 27017, all on localhost.